---
author: Guillermo Ferrer Bosque
title: Automation of web interactions
date: 2020-05-18
---

Resume
--------

Create a GabrielMessenger module to send publications through WhatsApp Web.

Gabriel Messenger
-----------------

Gabriel Messenger is an application that receives publications from some sources and send to your social networks.

Gabriel Messenger: Architecture
-------------------------------

Asyncio

 * Concurrent
 * I/O operations
 * Low requirements
 * Tasks that generate tasks. 

Gabriel Messenger: Asyncio vs Threads vs Multiprocessing
-------------------------------

![](https://files.realpython.com/media/Threading.3eef48da829e.png)

Gabriel Messenger: Asyncio vs Threads vs Multiprocessing
-------------------------------
![](https://files.realpython.com/media/Asyncio.31182d3731cf.png)


Gabriel Messenger: Diagram
-------------------------------
![](Diagram.png)

Gabriel Messenger: Diagram
-------------------------------
![](https://i.giphy.com/media/I2QQlj7vgtT6U/giphy.gif)


Gabriel Messenger: Shutdown
-------------------------------

 * SIGINT: Normal shutdown, checked by APP.
 * SIGTERM: Immediately shutdown, checked by Python Interpreter. Possibility of data loss.

 
Gabriel Messenger: Data Conversion
-------------------------------

 * Pandoc
 * Custom data conversion modules (using pandoc)
 

WhatsApp Web Module: Web Browser Controller
----------------

 * ~~Selenium~~
 * ~~Arsenic~~
 * ~~miyakogi/pyppeteer~~
 * pyppeteer/pyppeteer

WhatsApp Web Module: Web Browser Controller (Selenium)
---

![](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2019/05/Picture5.png)

Possibility of executing Selenium in a Thread.


WhatsApp Web Module: Web Browser Controller (Selenium)
---
![](https://thumbs.gfycat.com/DistantBitesizedChameleon-size_restricted.gif)


WhatsApp Web Module: Web Browser Controller (Arsenic)
---

Selenium, async version. On background works with threads. Chrome Driver and Geckodriver are **sync**. Last commit: 
14 Feb 2019


WhatsApp Web Module: Web Browser Controller (miyakogi/pyppeteer)
---

Last commit: 10 May 2019. Puppetter port (NodeJS).

![](https://davertmik.github.io/slides-puppeteer/img/protocol.png)

Problems with websockets. 


WhatsApp Web Module: Web Browser Controller (pyppeteer/pyppeteer)
---

Port of miyakogi/pyppeteer.


WhatsApp Web Module: CSS Selectors
---
  
| Selector           | Example     | Description                                               |
|--------------------|-------------|-----------------------------------------------------------|
| .class             | .intro      | Selects all elements with class="intro"                   |
| #id                | #firstname  | Selects the element with id="firstname"                   |
| element            | p           | Selects all \<p\> elements                                |

WhatsApp Web Module: CSS Selectors
---

| Selector           | Example     | Description                                               |
|--------------------|-------------|-----------------------------------------------------------|
| \*                 | \*          | Selects all elements                                      |
| element, element   | div, p      | Selects all \<div\> elements and all \<p\> elements       |
| .class.class       | .intro.test | Selects all elements whose class are "intro" and "test" |


WhatsApp Web Module: Attribute selectors
---

[attr] [attr=value] [attr~=value] [attr|=value] [attr^=value] [attr$=value] [attr*=value] [attr operator value i] 

input[data-icon="send-light"]

.intro[data-icon="send-light"]

\#a12[data-icon="send-light"]


WhatsApp Web Module: Accomplished
---

 * Send text.
 * Send images.
 * Send files.
 * Work with multiple cellphones concurrently.
 * Receiver module (Recycler).

WhatsApp Web Module: Failed
---

 * Send Session QR.
 * Deal with session (Partially).
 * Deal with disconnection (Partially).
 * Tests.
 * Dockerize.
 * ARM Support.

WhatsApp Web Module: Problems
---

 * Switch between different API/webdrivers.
 * Architecture problem
 * Difficult website. Nonesense class names. JS rules.
 * Pandoc compiling 
 
 
---

![](https://i.pinimg.com/736x/e4/63/70/e463703a7f95956034d91ad790f3c8a5.jpg)


