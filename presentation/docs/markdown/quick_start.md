# QuickStart

## Create configuration file

Configuration file is a yaml that must be named config.yaml. This file must be saved on
/etc/gabriel-messenger/config.yaml

See next example:

```
configuration:
  environment: test
  logging:
    global:
      level: "WARNING"
      fmt: "%(asctime)s %(process)d %(levelname)-8s %(name)s %(message)s"
    application:
      stream:
        formatter:
          fmt: "%(asctime)s %(process)d %(levelname)-8s %(name)s %(message)s"
        level: "INFO"
      smtp:
        formatter:
          fmt: "%(asctime)s %(process)d %(levelname)-8s %(name)s %(message)s"
        level: "ERROR"
        sender: "username@domain.tld"
        recipients: ["username@domain.tld"]
        subject: "Subject"
        username: "username"
        password: "password"
        hostname: 'serverhostname'
        port: 465
        use_tls: True
      file:

environment:
  production:
    sender:
      WhatsApp Web:
        headless: false
        instances:
          Guibos:
  test:
    sender:
      WhatsApp Web:
        headless: false
        instances:
          Guibos:
    receiver:
      Recycler:
        - senders:
          WhatsApp Web:
            Guibos:
              channel-name:
                publication_data:
                  colour: 16711680
          module_config:
            wait_time: 20
            publications:
              - publication_id: 1
                title: {format_data: markdown, data: '# Title'}
```

* **Configuration**: Global configuration. This not change between environments.
  * **Environment**
  * **Logging**: Logging Config
    * **Global**: Affect all external libraries.
    * **Application**: Affect only GabrielMessenger modules.
      * **Stream**: Stout and Stderr output logging.
        * **Formatter**
          * **fmt**
        * **Level**: Logging level.
    * **SMTP**: SMTP output logging:
      * **Level**: Logging level.
      * **Sender**
      * **Recipients**
      * **Subject**
      * **Username**
      * **Password**
      * **Hostname**: SMTP hostname server
      * **Port**
      * **Use_tls**
    * **File**: No require more configuration. Add key. To save all logs in a file. Each instance of application or a service have different files.



* **Environment**: Node that save all environments.
  * **Environment node**: To know what environment are available, check this: `src.ser.common.enums.environment.Environment()`
    * **Sender Section**: (Sender) This section save all configuration of each *Sender Service*. If you are not interested to load some *Sender Service*, not write (or comment) his sender section.
      * **Sender Configuration**: (In this case WhatsApp Web) This configure a sender service if you want to know how to configure each *Sender Service* check this document Modules.
    * **Receiver Section**: This section save all configuration of each *Receiver Service*. If you are not interested to load some *Receiver Service*, not write (or comment) his sender section.
      * **Senders**: Section where is configured where publications will be send.
        * **Sender Service Name**: (In this case WhatsApp Web) will be the same as **Sender Configuration** Key.
          * **Channel name or channel id**: (In this case “Guibos” or “Gabriels”) Is a id or str of a channel or group that you want send publications. To get more information for each sender service check: Modules.
            * **publication_data**: This data will add or replace values of a publication. In previous example uses title replacement or title adding. `src.ser.common.itf.publication.Publication()`
      * **module_config**: Configuration of the *Receiver Module*. To get more information for each receiver service check: Modules.

### Good Practices

One of the biggest practise is create yaml anchors to avoid duplicated configuration.

```
aliases:
  test:
    senders: &test_sender
      WhatsApp Web:
        Guibos:
          GTEST-1:
            publication_data:
              colour: 16711680
              author:
                name: 'Tester'
                url: 'http://example.com'
                icon_url: "hhttp://example.com/example.jpg"
environment:
  production:
    receiver:
      Blackfire:
        - senders:
            << :  [ *test_sender ]
          ...
```

For more information this [document](http://blogs.perl.org/users/tinita/2019/05/reusing-data-with-yaml-anchors-aliases-and-merge-keys.html) is a good reference.

## Create application environment

Currently the are two main ways to execute this app:


* Run a docker image


* Create your own environment

The easy way to use this app is run a docker image.

### Run a docker image

This method is the easy way. [Docker image](https://hub.docker.com/r/guibos/gabriel-messenger) will be update of
every gabriel-messenger update.

#### Install docker

You must install [docker](https://docs.docker.com/get-docker/).

#### Run container

```
docker run -v <your-configuration-directory>:/etc/gabriel-messenger guibos/gabriel-messenger
```

Take care that <your-configuration-directory> is a directory in your docker host with your config.yaml

### Create your own environment

Use this Dockerfile as an idea that you will need:

```
FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install python3.8 python3-pip python3.8-dev git pandoc -y

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN git clone https://github.com/guibos/gabriel-messenger.git /opt/gabriel-messenger
WORKDIR /opt/gabriel-messenger

RUN pip3 install pipenv
RUN pipenv install

ENTRYPOINT ["pipenv", "run", "run"]
```
