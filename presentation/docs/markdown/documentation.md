# Documentation

## Definitions


* **Publication**: A publication is a dataclass that includes all the information necessary to generate an entry on any social network. A publication will be shared by a **receiver service** to n **\*sender services\***.


* **Service**: A service is a utility that allow to manage publications in some way. There are two types of services: “**Receivers**” and “**Senders**”.
  * **Receiver Service**: A service that download information from external resources and generate publications. This publications will be upload in configured queues of **sender services**.
  * **Sender Service**: A service that get publications from his queue and publish to external resource.


* **Task**: Tasks are used to schedule coroutines concurrently. See: [asyncio task](https://docs.python.org/3/library/asyncio-task.html).
  * All services will generate one or more task (normally one) to perform his functionality.
  * Other tasks: to manage state of services. Currently only to perform shutdown of all services. See next code:


### class src.app.application.Application(configuration: src.inf.configuration.configuration.Configuration)
Application class. The one in charge of governing all the modules.


#### _clean_shutdown()
Handler that will be activated when app receives a SIGINT signal. This create a task to programming a clean
shutdown.

## Workflow

![](static/workflow.png)

Each **receiver Service** download data from external resource, create publications with this data and send this publications to each queue configured. One queue, one *Sender Service*. Sender get publications from his queue and send to external resource.

