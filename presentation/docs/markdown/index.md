# Gabriel Messenger

*Create your publications and publish them on all your social networks.*

Documentation here: [https://guibos.github.io/gabriel-messenger/](https://guibos.github.io/gabriel-messenger/)

## Introduction

Gabriel Messenger is a application that receives publications from some sources and send to your social networks.

Official modules:


* Receivers
  * [ADC Blackfire](https://www.blackfire.eu/)
  * [Facebook](https://www.facebook.com/)
  * [Google Calendar](https://calendar.google.com/)
  * [Weiß Schwarz - Banners EN/JP](https://en.ws-tcg.com/)
  * [Weiß Schwarz - Today’s Card EN/JP](https://en.ws-tcg.com/products/ws_today)


* Senders
  * [Discord](https://discordapp.com/)
  * [WhatsApp](web.whatsapp.com/)

## Index
* [QuickStart](quick_start.md)
  * Create configuration file
    * Good Practices
  * Create application environment
    * Run a docker image
      * Install docker
      * Run container
    * Create your own environment
* [Documentation](documentation.md)
  * Definitions
  * Workflow
* [Modules](modules.md)
  * Sender Services
    * WhatsApp Web
      * Features
      * Configuration
      * Technical Notes
      * Class
      * Flowchart
   * Receiver Services
     * Recycler
     * Configuration
* [Developer](developer.md)
  * Architecture
  * Main Classes
    * Interfaces
    * Mixins
    * Enums
  * Testing
    * Run application
    * Force environment
* [TODO](todo.md)
  * Publication
    * Blogger
    * Discord
    * Facebook Sender
    * Facebook Receiver
    * Instagram
    * Twitter
    * WhatsApp Web
