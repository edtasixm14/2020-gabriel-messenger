# Developer

## Architecture

You will need to implement your new service with this architecture:

![](static/architecture.png)

## Main Classes

### Interfaces


#### class src.ser.common.itf.publication.Publication(publication_id: Union[int, str, None] = None, title: Optional[src.ser.common.rich_text.RichText] = None, description: Optional[src.ser.common.rich_text.RichText] = None, url: Optional[str] = None, timestamp: Optional[datetime.datetime] = None, colour: Optional[int] = None, images: List[src.ser.common.value_object.file_value_object.FileValueObject] = <factory>, files: List[src.ser.common.value_object.file_value_object.FileValueObject] = <factory>, author: Optional[src.ser.common.value_object.author.Author] = None, custom_fields: Optional[src.ser.common.itf.custom_fields.CustomFields] = None)
Publication Interface. Is the base to create another dataclass that will be used to share publications between
services.

### Mixins


#### class src.ser.common.service_mixin.ServiceMixin()
Common Service Mixin. This class includes methods that required by senders services and receivers services.


#### class src.ser.common.receiver_mixin.ReceiverMixin(receiver_full_config: src.ser.common.value_object.receiver_full_config.ReceiverFullConfig)
Receiver Common Service Mixin. This mixin include methods required by receivers services.


#### class src.ser.common.sender_mixin.SenderMixin(state_change_queue: asyncio.queues.Queue, logger: src.inf.logger.itf.logger_interface.LoggerInterface, publication_queue: asyncio.queues.Queue, failed_publication_directory: str)
Sender Common Service Mixin. This mixin include methods required by senders services.

### Enums


#### class src.ser.common.enums.environment.Environment()
Environment enum.

## Testing

### Run application

```
pipenv run run
```

### Force environment

You can switch your environment without changing your configuration file. See next code:

```
pipenv run run --environment <environment>
```
