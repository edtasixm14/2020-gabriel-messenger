# Modules

## Sender Services

### WhatsApp Web

WhatsApp Web Module uses WhatsApp Web service to communicate with your cellphone through a web browser.

Do not use this module if you need high availability. If you require high availability see:
[WhatsApp API](https://www.whatsapp.com/business/api). This module is subject to problems of all kinds.
This module is intended for those individuals, companies or associations that cannot cover the expenses of
[WhatsApp API](https://www.whatsapp.com/business/api).

#### Features

This module allow:


* Send text messages
* Send images
* Send files
* Send with different cellphone numbers.

This module not allow:
* Send videos

#### Configuration

```
environment:
  production:
    sender:
      WhatsApp Web:
        headless: true
        instances:
          Guibos:
          Gabriel:
```

“Guibos” and “Gabriel” will be different instance of WhatsApp Web module. Previous key don’t have any special meaning.
These keys are used by Receiver Services to discriminate different instances. Each instance will have his own web
browser configuration. In each instance you must add QR code on startup. For now you will disable headless mode to add
new. Each instance must have configured different cellphone numbers.

#### Technical Notes

This module uses [pyppeteer2](https://github.com/pyppeteer/pyppeteer2). This library is async, that work with
chromium browser. [Pyppeteer2](https://github.com/pyppeteer/pyppeteer2) controls chromium browser through
[websockets](https://www.w3.org/TR/2009/WD-websockets-20091222/).
This module simulate what is doing a person when he interact with WhatsApp Web. To chose a html element,
[selectors](https://www.w3.org/TR/selectors-api/) are used.

#### Class


### class src.ser.whats_app_web.service.WhatsAppWebService(\*, publication_queue: asyncio.queues.Queue, state_change_queue: asyncio.queues.Queue, logger: src.inf.logger.itf.logger_interface.LoggerInterface, failed_publication_directory: str)
WhatsApp Web Client.


#### async run(data_directory: str, headless: bool)
Run service

#### Flowchart

![](static/whatsapp_flowchart.png)

## Receiver Services

### Recycler

Recycler service get publications from config and send one of them between time specified in wait_time field. Take
care that *Recycler Service* create a instance for each of items of the main list. So you can send different
publications to different *Senders Services* and different recipients.

#### Configuration

```
environment:
  production:
    receiver:
      Recycler:
        - senders:
            Discord:
              publisher:
                000:
                  publication_data:
                    colour: 16711680
                    author:
                      name: 'Example'
                      url: 'https://example.com'
                      icon_url: "https://example.com/example.jpg"
            WhatsApp Web:
              Guibos:
                Channel:
                  publication_data:
                    colour: 16711680
                    author:
                      name: 'Example'
                      url: 'https://example.com'
                      icon_url: "https://example.com/example.jpg"
          module_config:
            wait_time: 20
            publications:
              - publication_id: 1
                title: {format_data: markdown, data: 'Na, na, na, na, na, na, na'}
                description: {format_data: markdown, data: 'Na, na, na, na, na, na, na'}
                url: 'http://example.com'
                timestamp: '2000-01-01T00:00:00'
                colour: 0
                images:
                  - path: 'image1.jpg'
                  - path: 'image2.jpg'
                files:
                  - path: 'file1.pdf'
                  - path: 'file1.zip'
                author:
                  name: 'Example'
                  url: 'https://example.com'
                  icon_url: "https://example.com/example.jpg"
              - publication_id: 2
                title: {format_data: markdown, data: 'Na, na, na, na, na, na, na'}
        - senders:
            Discord:
              publisher:
                000:
                  publication_data:
                    colour: 16711680
                    author:
                      name: 'Example'
                      url: 'https://example.com'
                      icon_url: "https://example.com/example.jpg"
            module_config:
              wait_time: 20
              publications:
                - publication_id: 2
                  title: {format_data: markdown, data: 'Na, na, na, na, na, na, na'}
```

Module config have a list of publications. Is not necessary to complete all fields.
