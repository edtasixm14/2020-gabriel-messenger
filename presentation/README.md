# Presentació 2020 Guillermo Ferrer

## Projecte:

El projecte finalment elegit va ser:

CI Selenium IDE: Eina d'open source per automatització de proves que intenten simular
interaccions de les persones en llocs web, d'aquesta manera s'elimina la necessitat de
fer un pas manual repetitiu.

La proposta va ser:

Crear mòdul per GabrielMessenger per poder enviar missatges a través de WhatsApp Web.

GabrielMessenger (abans GameManager, i possiblement és canvi a Leliel) és una aplicació per la gestió de xarxes socials.
És una aplicació asíncrona i extremadament modular, gestionada a través de configuració.

![](https://ih0.redbubble.net/image.88699509.8123/mp,550x550,matte,ffffff,t.u7.jpg)

Els fitxers que proporcionen principalment la funcionalitat d'enviar missatges són:

* src/ser/whats_app_web/service.py
* src/ser/common/rich_text.py

Per poder testejar i demostrar la funcionalitat de forma fàcil es va crear el mòdul ***Recycler***:

* src/ser/recycler/service.py

Aquest mòdul genera publicacions de forma periòdica. Aquestes publicacions es generen a través del `config.yaml`

A causa de dos problemes amb l'arquitectura de GabrielMessenger es va haver de modificar quasi tots els fitxers. Aquesta
modificació ja estava prevista però no en el moment de fer el projecte.

* Obtenció de la configuració de fitxer `config.yaml`. De la manera que s'obtenia la configuració feia difícil la implementació de ***Recycler***.
* Modificar com les instàncies guardaven les dades. Aquestes dades podien ser fitxer, configuracions, bd, etc.
Abans instàncies del mateix mòdul guardaven les dades en el mateix directori, fins i tot compartien el mateix fitxer
SQLite. Cada instància de WhatsApp Web necessita directoris diferents per guardar cada una de les configuracions de
Chrome. Ara cada instància té el seu propi directori.

Totes les modificacions es poden veure: [https://github.com/guibos/gabriel-messenger/commits/master](https://github.com/guibos/gabriel-messenger/commits/master)
a partir del commit: 'Add WhatsApp Web module, change config manager, improve receiver opti...'

## Pòster

[Pòster](https://docs.google.com/document/d/1GygSZKGmjyji9VytS-QJXDyX9qUfRrH1tjK8XLPJY-o/edit?usp=sharing)

## Vídeo
* [Vídeo aplicació](video.mkv)
* [Vídeo comercial](https://app.wideo.co/en/view/28763071588883946754-gabrielmessenger?html5=true)

## Documentació
Tota la documentació de GabrielMessenger es va fer dins del calendari del projecte de CFGS. Encara no està completa, ja que
falta bastant a documentar. Està documentat de forma profunda la part del mòdul de "WhatsApp Web", i tot allò que té
una relació directa amb el mòdul de "WhatsApp Web", configuració, arquitectura, etc.

La documentació es fa fer originalment amb ReST + Sphinx, ja que té molts avantatges en documentar un codi Python. Amb
l'aprovació del tutor. Hi ha tres versions: ReST + Sphinx, HTML i Markdown. La build HTML i MD està feta a partir de
ReST + Sphinx. La build en Markdown hi han errors que s'han de solucionar a mà, perquè és possible que en el
futur; la versió en MD, quedi desfasada.

* [Documentació en Markdown](docs/markdown/index.md)
* [Documentació ReStructuredText](https://github.com/guibos/gabriel-messenger/tree/master/docs/source)
* [Documentació HTML](https://guibos.github.io/gabriel-messenger/)

## Presentació

És possible que la [presentació](https://guibos.github.io/presentation/) hi hagi imatges tallades però la part
tallada no és important.

Generate HTML:
```
pandoc -t revealjs -s -o docs/slides/index.html docs/slides/slides.md -V revealjs-url=https://revealjs.com
```